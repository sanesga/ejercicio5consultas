package app;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import model.AsistenciaMedica;
import model.Coberturas;
import model.Direccion;
import model.Enfermedades;
import model.Nif;
import model.Seguro;
import model.Sexo;
import model.TipoAsistencia;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Crea un nuevo programa similar al Ejemplo01 que realice las operaciones de
 * insertar, leer, actualizar y borrar de la entidad Seguro usando los ficheros
 * ”.hbm.xml” de Hibernate.
 */
public class main {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        Seguro s = null;
        Direccion d = null;
        int opcion;
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        Set<AsistenciaMedica> asistenciasMedicas = new HashSet<>();
        AsistenciaMedica am1, am2;
        Nif nif;
        Enfermedades e;
        Coberturas c;
        BigDecimal bd1, bd2;
        String texto1, texto2;
        Sexo sexo = null;
        TipoAsistencia tipoAsistencia = null;
        Query query = null;
        List<Seguro> seguros;
        List<Object[]> listaObjetos;
        List<Object> objeto;
        List<AsistenciaMedica> listaAsistenciasMedicas;

        //configuration.addClass(Seguro.class);
        configuration.configure();

        sessionFactory = configuration.buildSessionFactory();

        //creamos una sesion
        Session session = sessionFactory.openSession();

        do {

            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR SEGURO
                    nif = new Nif("123456V");
                    e = new Enfermedades(false, false, false, true, "polvo");
                    c = new Coberturas(true, true, false);
                    s = new Seguro(nif, "Juan", "Cano", "Morales", 38, 3, c, e, sexo.HOMBRE);
                    d = new Direccion(7, "Calle la Reina", 5, "Xàtiva", "Valencia");
                    bd1 = new BigDecimal("5.5");
                    bd2 = new BigDecimal("10.2");
                    texto1 = "Una temperatura normal puede variar de persona a persona, pero generalmente es alrededor de 98.6 ºF o 37ºC. La fiebre no es una enfermedad. Por lo general, es una señal de que su cuerpo está tratando de combatir una enfermedad o infección. Las infecciones causan la mayoría de las fiebres.";
                    texto2 = "Los signos y síntomas de la apendicitis pueden comprender los siguientes: Dolor repentino que comienza en el lado derecho de la parte inferior del abdomen. Dolor repentino que comienza alrededor del ombligo y, a menudo, se desplaza hacia la parte inferior derecha del abdomen";
                    am1 = new AsistenciaMedica(s, "Ir al médico de cabecera por fiebre", "Valencia", texto1, bd1, tipoAsistencia.Ambulatoria);
                    am2 = new AsistenciaMedica(s, "Operación de apendicitis", "Castellón", texto2, bd2, tipoAsistencia.CentroSalud);
                    asistenciasMedicas.add(am1);
                    asistenciasMedicas.add(am2);
                    s.setAsistenciasMedicas(asistenciasMedicas);
                    s.setDireccion(d);
                    System.out.println("Seguro creado con éxito");
                    break;
                case 2://GUARDAR SEGURO
                    session.beginTransaction();
                    session.save(s);
                    session.getTransaction().commit();
                    System.out.println("Seguros guardado con éxito");
                    break;
                case 3://LEER SEGURO
                    s = session.get(Seguro.class, 51);
                    System.out.println(s);
                    break;
                case 4://ACTUALIZAR
                    session.beginTransaction();
                    s.setApe1("Rodríguez");
                    session.update(s);
                    session.getTransaction().commit();
                    System.out.println("Seguro actualizado con éxito");
                    break;
                case 5://ELIMINAR SEGURO
                    session.beginTransaction();
                    session.delete(s);
                    session.getTransaction().commit();
                    System.out.println("Seguro eliminados con éxito");
                    break;
                case 6://. Consulta 1. Todos los seguros que hay en la base de datos
                    query = session.createQuery("SELECT s FROM Seguro s");

                    seguros = query.list();

                    for (Seguro seguro : seguros) {
                        System.out.println(seguro);
                    }
                    break;
                case 7://Consulta 2. Retornar el NIF y nombre de todos los seguros de la base de datos
                    query = session.createQuery("SELECT s.nif, s.nombre FROM Seguro s");

                    listaObjetos = query.list();

                    for (Object[] listaObjeto : listaObjetos) {
                        System.out.println(listaObjeto[0] + "--" + listaObjeto[1]);
                    }
                    break;
                case 8://Consulta 3. Retornar el NIF de todos los seguros de la base de datos
                    query = session.createQuery("SELECT s.nif FROM Seguro s");

                    objeto = query.list();

                    for (Object object : objeto) {
                        System.out.println(object);
                    }
                    break;
                case 9:// Consulta 4. Retornar el NIF para el seguro con el nombre 'Marcos Tortosa Oltra' --CON NOMBRE--
                    //Nif numeroNif= (Nif) session.createQuery("SELECT s.nif FROM Seguro s WHERE s.nombre='Marcos' AND ape1='Tortosa' AND ape2='Oltra'").uniqueResult();
                    //System.out.println(numeroNif);
                   
                    String nombre = "Marcos";
                    String ape1="Tortosa";
                    String ape2="Oltra";

                    Object object = session.createQuery("SELECT s.nif FROM Seguro s WHERE s.nombre=:nombre AND ape1=:ape1 AND ape2=:ape2").setString("nombre", nombre).setString("ape1", ape1).setString("ape2", ape2).uniqueResult();
                   
                    System.out.println(object);
                    
                    break;
                case 10://Consulta 5. Retornar AsistenciasMedicas con importe mayor o igual a 10000 --CON NOMBRE--
                    int importe = 10000;
                    
                    query = session.getNamedQuery("grandesGastos");
                    query.setInteger("importe", importe);
                    
                    listaAsistenciasMedicas = query.list();
                    for (AsistenciaMedica a : listaAsistenciasMedicas) {
                        System.out.println(a);
                    }
                    break;
                case 11://Ejecuta la consulta grandesGastos y muestra el id de AsistenciaMedica
                     int importe2 = 10000;
                    
                    query = session.getNamedQuery("grandesGastos");
                    query.setInteger("importe", importe2);
                    
                    listaAsistenciasMedicas = query.list();
                    for (AsistenciaMedica a : listaAsistenciasMedicas) {
                        System.out.println(a.getIdAsistenciaMedica());
                    }
                    
                    break;
                case 12://Retornar el id de AsistenciaMedica de todas las Asistencias Médica con saldo entre 2000 y 5000 euros. --POR POSICIÓN--
                     int min=2000;
                     int max=5000;
                   
                   query = session.createQuery("SELECT a.idAsistenciaMedica FROM AsistenciaMedica a WHERE a.importe BETWEEN ?0 AND ?1");
                    
                   query.setInteger(0, min);
                   query.setInteger(1, max);
                   
                   objeto=query.list();
                   
                    for (Object o : objeto) {
                        System.out.println(o);
                    }
                   
                    break;
                case 13://Calcular la suma de todos los importes de todas las asistencias médicas
                    BigDecimal suma = (BigDecimal) session.createQuery("SELECT SUM(a.importe) FROM AsistenciaMedica a").uniqueResult();
                    System.out.println(suma);
                    break;
                case 14://Calcular el saldo medio de todas las Asistencias Médicas.
                    Double saldoMedio = (Double) session.createQuery("SELECT AVG(a.importe) FROM AsistenciaMedica a").uniqueResult();
                    System.out.println(saldoMedio);
                    break;
                case 15://Calcular cuántos seguros hay
                    Long total = (Long) session.createQuery("SELECT COUNT(s) FROM Seguro s").uniqueResult();
                    System.out.println(total);
                    break;
                case 16://Mostrar para cada seguro, cuántas asistencias médicas posee

                    query = session.createQuery("SELECT s.idSeguro, COUNT(a.seguro) FROM AsistenciaMedica a, Seguro s WHERE a.seguro = s.idSeguro GROUP BY a.seguro");
                    
                     listaObjetos = query.list();
                    for (Object[] o : listaObjetos) {
                        System.out.println(o[0] + "--" + o[1]);
                    }
                    
                    break;
                case 17://Obtener todos los objetos seguro y las alergias que padecen
                  
                    query = session.createQuery("SELECT s, s.enfermedades FROM Seguro s");
                    
                     listaObjetos = query.list();
                    for (Object[] o : listaObjetos) {
                        Seguro seg = (Seguro) o[0];
                        Enfermedades enf = (Enfermedades) o[1];
                        
                        System.out.println(seg + "--" + enf.getNombreAlergia());
                    }
                    break;
                case 18://Retornar todos los seguros y el id de asistencias médicas de cada asistencia médica
                     query = session.createQuery("SELECT s, a.idAsistenciaMedica FROM Seguro s, AsistenciaMedica a WHERE s.idSeguro = a.seguro");
                    
                    listaObjetos = query.list();
                    for (Object[] o : listaObjetos) {
                        System.out.println(o[0] + "--" + o[1]);
                    }
                    break;
                case 19://La misma consulta de antes pero minimizando el número de consultas que lanza hibernate
                     query = session.createQuery("SELECT s, a.idAsistenciaMedica FROM Seguro s, AsistenciaMedica a WHERE s.idSeguro = a.seguro GROUP BY s");
                    
                    listaObjetos = query.list();
                    for (Object[] o : listaObjetos) {
                        System.out.println(o[0] + "--" + o[1]);
                    }
                    break;
                case 20://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 20);
    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR LOS SEGUROS, COMO SON FIJOS DARÁ ERROR");
        System.out.println("CONSIDERACIÓN 4. BORRAR EL SEGURO ANTES DE SALIR, PARA PODER UTILIZAR LOS MISMOS DATOS MÁS TARDE");
        System.out.println("1. Crear seguro");
        System.out.println("2. Guardar seguro");
        System.out.println("3. Leer seguro");
        System.out.println("4. Actualizar seguro");
        System.out.println("5. Eliminar seguro");
        System.out.println("-----------------------CONSULTAS------------------------");
        System.out.println("6. Consulta 1. Todos los seguros que hay en la base de datos");
        System.out.println("7. Consulta 2. Retornar el NIF y nombre de todos los seguros de la base de datos");
        System.out.println("8. Consulta 3. Retornar el NIF de todos los seguros de la base de datos");
        System.out.println("9. Consulta 4. Retornar el NIF para el seguro con el nombre 'Marcos Tortosa Oltra' --CON NOMBRE--");
        System.out.println("10. Consulta 5. Retornar asistencias médicas con importe mayor o igual a 10000 --CON NOMBRE--");

        System.out.println("--------------------------NO HECHAS-------------------------");
        System.out.println("11. Consulta 6. Ejecuta la consulta grandesGastos y muestra el id de asistencia medica");
        System.out.println("12. Consulta 7. Retornar el id de asistencia médica de todas las asistencias médiéas con saldo entre 2000 y 5000 euros. --POR POSICIÓN--");
        System.out.println("------------------------------------------------------------");

        System.out.println("13. Consulta 8. Calcular la suma de todos los importes de todas las asistencias médicas");
        System.out.println("14. Consulta 9. Calcular el saldo medio de todas las asistencias médicas.");
        System.out.println("15. Consulta 10. Calcular cuántos seguros hay");
        System.out.println("16. Consulta 11. Mostrar para cada seguro, cuántas asistencias médicas posee");
        System.out.println("17. Consulta 12. Obtener todos los objetos seguro y las alergias que padecen");
        System.out.println("18. Consulta 13. Retornar todos los seguros y el id de asistencias médicas de cada asistencia médica");
        System.out.println("19. Consulta 14. La misma consulta de antes pero minimizando el número de consultas que lanza hibernate");
        
        System.out.println("20. Salir");
    }
}
