/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sandra
 */
public class Coberturas {
    private boolean oftalmologia;
    private boolean dental;
    private boolean fecundacioninVitro;

    public Coberturas(boolean oftalmologia, boolean dental, boolean fecundacioninVitro) {
        this.oftalmologia = oftalmologia;
        this.dental = dental;
        this.fecundacioninVitro = fecundacioninVitro;
    }
    

    public Coberturas() {
    }

    @Override
    public String toString() {
        return "Coberturas{" + "oftalmologia=" + oftalmologia + ", dental=" + dental + ", fecundacioninVitro=" + fecundacioninVitro + '}';
    }
    

    public boolean isOftalmologia() {
        return oftalmologia;
    }

    public void setOftalmologia(boolean oftalmologia) {
        this.oftalmologia = oftalmologia;
    }

    public boolean isDental() {
        return dental;
    }

    public void setDental(boolean dental) {
        this.dental = dental;
    }

    public boolean isFecundacioninVitro() {
        return fecundacioninVitro;
    }

    public void setFecundacioninVitro(boolean fecundacioninVitro) {
        this.fecundacioninVitro = fecundacioninVitro;
    }
    
    
}
