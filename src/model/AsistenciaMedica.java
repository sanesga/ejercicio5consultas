/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Sandra
 */
public class AsistenciaMedica {

    private int idAsistenciaMedica;
    private Seguro seguro;
    private String breveDescripcion;
    private String lugar;
    private String explicacion;
    private TipoAsistencia tipoAsistencia;
    private Date fecha;
    private Date hora;
    private BigDecimal importe;

    public AsistenciaMedica(Seguro seguro, String breveDescripcion, String lugar, String explicacion, BigDecimal importe, TipoAsistencia tipoAsistencia) {

        this.seguro = seguro;
        this.breveDescripcion = breveDescripcion;
        this.lugar = lugar;
        this.explicacion=explicacion;
        this.importe = importe;
        this.fecha = Calendar.getInstance().getTime();
        this.hora = Calendar.getInstance().getTime();
        this.tipoAsistencia =tipoAsistencia;
    }

    public AsistenciaMedica() {
    }

    @Override
    public String toString() {
        return "AsistenciaMedica{" + "idAsistenciaMedica=" + idAsistenciaMedica + ", seguro=" + seguro + ", breveDescripcion=" + breveDescripcion + ", lugar=" + lugar + ", explicacion=" + explicacion + ", tipoAsistencia=" + tipoAsistencia + ", fecha=" + fecha + ", hora=" + hora + ", importe=" + importe + '}';
    }

    

    public String getExplicacion() {
        return explicacion;
    }

    public void setExplicacion(String explicacion) {
        this.explicacion = explicacion;
    }

    public TipoAsistencia getTipoAsistencia() {
        return tipoAsistencia;
    }

    public void setTipoAsistencia(TipoAsistencia tipoAsistencia) {
        this.tipoAsistencia = tipoAsistencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }
    

    public int getIdAsistenciaMedica() {
        return idAsistenciaMedica;
    }

    public void setIdAsistenciaMedica(int idAsistenciaMedica) {
        this.idAsistenciaMedica = idAsistenciaMedica;
    }

    public String getBreveDescripcion() {
        return breveDescripcion;
    }

    public void setBreveDescripcion(String breveDescripcion) {
        this.breveDescripcion = breveDescripcion;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Seguro getSeguro() {
        return seguro;
    }

    public void setSeguro(Seguro seguro) {
        this.seguro = seguro;
    }
}
